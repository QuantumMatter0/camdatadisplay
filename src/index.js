import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import blue from "@material-ui/core/colors/blue";

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import { SocketTest } from "./pages/socket_test";
import Logo from "./res/AQ_logo.png";

function notFound() {
    return (
        <div className="notFound">
            <Typography variant="display4">
                404
            </Typography>
            <Typography variant="display2">
                Page Not Found
            </Typography>
        </div>
    );
}

const theme = createMuiTheme({
    palette: {
        primary: {
            main: "#FFFFFF"
        }
        
    },
});

function Main() {
    return (
        <MuiThemeProvider theme={theme}>
            <AppBar position="sticky">
                <Toolbar color="primary">
                    <img className="logo" src={ Logo } />
                    <Typography variant="title" color="inherit">
                        Live CAM Data
                    </Typography>
                </Toolbar>
            </AppBar>
            <Grid container>
                <Grid item xs/>
                <Grid item sm={10} xs={12}>
                    <Router>
                        <div className="router_container">
                            <Switch>
                                <Route exact path="/" component={ App }/>
                                <Route path="/cam/:id" component={ SocketTest } />
                                <Route path="/cam" component={ SocketTest } />
                                <Route component={ notFound } />
                            </Switch>
                        </div>
                    </Router>
                </Grid>
                <Grid item xs/>
            </Grid>
        </MuiThemeProvider>
    );
}

ReactDOM.render(<Main />, document.getElementById('root'));
registerServiceWorker();
