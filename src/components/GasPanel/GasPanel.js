import './GasPanel.css'

import React from "react";
import Measure from "react-measure"

import  * as AQI from "../../classes/aqi_calculator";

import { TimeAgo } from "../TimeAgo/TimeAgo";

export class GasPanel extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            dimensions: {
                width: -1,
                height: -1,
            },
        }

        this.canvas = React.createRef();
    }

    componentDidMount() {
        this.updateCanvas();
    }

    componentDidUpdate() {
        this.updateCanvas();
    }

    updateCanvas() {
        const ctx = this.canvas.current.getContext("2d");
        const { width, height } = this.state.dimensions;
        const offset = 0.15 * height;
        const graphHeight = height * 0.7;
        
        const aqi = AQI.AQI(this.props.data.points.slice(-1)[0].value, this.props.data.name);
        const colors = AQI.AQI_Colors[aqi];
        const primary = colors.primary;
        const secondary = colors.secondary;

        ctx.fillStyle = primary;//AQI.AQI_Color(200, "CO2");
        ctx.fillRect(0, 0, width, height);

        ctx.fillStyle = secondary;
        ctx.beginPath();
        ctx.moveTo(0, height);

        let points = this.props.data.points.slice(-5);
        var min = 999999;
        var max = -999999;
        for (let i = 0; i < points.length; i++) {
            let point = points[i];
            if (point.value < min) { min = point.value; }
            if (point.value > max) { max = point.value; }
        }

        if (min === max) {
            min -= 1;
            max += 1;
        }

        var dy = graphHeight / (max - min);
        var dx = width / (points.length - 1);
        for (let i = 0; i < points.length; i++) {
            let point = points[i];
            let x = i * dx;
            let y = height - (offset + (point.value - min) * dy);
            ctx.lineTo(x, y);
        }
        ctx.lineTo(width, height);

        ctx.shadowBlur = 5;
        ctx.shadowColor = "black";
        ctx.fill();
    }

    render() {
        const aqi = AQI.AQI_Raw(this.props.data.points.slice(-1)[0].value, this.props.data.name);

        let lastPoint = this.props.data.points.slice(-1)[0];
        let lastDate = lastPoint.timestamp;

        return (
            <Measure bounds onResize={(contentRect) => { this.setState({ dimensions: contentRect.bounds }) }}>
                {({measureRef}) =>
                    <div ref={ measureRef }>
                        <div className="panel_container">
                            <canvas ref={ this.canvas } className="panel_canvas" width={ this.state.dimensions.width } height={ this.state.dimensions.height }/>
                            <div className="panel_content">
                                <p className="panel_title_label">{ this.props.data.name }</p>
                                <div className="panel_value_container">
                                    <p className="panel_value_label">{ this.props.data.points.slice(-1)[0].value } { this.props.data.units }</p>
                                </div>
                                { aqi >= 0 &&
                                    <p className="panel_subtitle_label">AQI: { aqi.toFixed(2) }</p>
                                }
                                {/* <p className="panel_time_label">{ dateStr }</p> */}
                                <TimeAgo className="panel_time_label" date={ lastDate } live={true}/>
                            </div>
                        </div>
                    </div>
                }
            </Measure>
        );
    }
}