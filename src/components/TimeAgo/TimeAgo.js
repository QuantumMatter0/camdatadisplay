import React from "react";

export class TimeAgo extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            update: true,
        };
    }

    componentDidMount() {
        if (this.props.live) {
            setTimeout(() => {
                this.tick();
            }, 5 * 1000);
        }
    }

    tick() {

        this.setState({
            update: !this.state.update,
        });

        if (this.props.live) {
            setTimeout(() => {
                this.tick();
            }, 5 * 1000);
        }
    }

    timeAgoString() {
        let diff = ((new Date()).getTime() - this.props.date.getTime()) / 1000;
        let value = "EMPTY";

        if (diff < 15) {
            value = "Just Now";
        } else if (diff < 60) {
            value = diff.toFixed(0) + " Seconds Ago";
        } else if (diff < 60 * 60) {
            value = (diff / 60).toFixed(0) + " Minutes Ago";
        } else if (diff < 60 * 60 * 24) {
            value = (diff / (60 * 60)).toFixed(0) + " Hours Ago";
        } else if (diff < 60 * 60 * 24 * 30) {
            value = (diff / (60 * 60 * 24)).toFixed(0) + " Days Ago";
        }
        return value;
    }


    render() {
        return (
            <p className={ this.props.className }>{ this.timeAgoString() }</p>
        );
    }

}