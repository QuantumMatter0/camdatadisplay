#!/home/aircd/env python3

from flask import Flask
from flask import send_from_directory

import os
import os.path

app = Flask(__name__)

ROOT_DIR = './'


@app.route('/')
def default():
    return deliver_angular()


@app.route('/<path:path>')
def with_path(path):
    return deliver_angular(path)


def deliver_angular(path=''):
    if os.path.isfile('./' + ROOT_DIR + path):
        return send_from_directory(ROOT_DIR, path)
    else:
        return send_from_directory(ROOT_DIR, 'index.html')

if __name__ == "__main__":
    app.run(host='web3.citizenair.org', port=8080)
