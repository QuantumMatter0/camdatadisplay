import React from 'react';
import ReactDOM from 'react-dom';
import './socket_test.css'

import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import 'typeface-roboto'

import { AirDB } from "../classes/airdb";
import { GasPanel } from '../components/GasPanel/GasPanel';

import AppleStoreBadge from "../res/app_store/apple.svg";
import AndroidStoreBadge from "../res/app_store/android.png"

import { Link } from "react-router-dom";

export class SocketTest extends React.Component {

    priorities = ["PM, 1µm", "PM, 2.5µm", "PM, 10µm", "CO", "CO₂", "Ozone", "Temperature", "Pressure", "Humidity"]
    
    constructor(props) {
        super(props);

        this.cam_id = 22;
        if (this.props.match.params.id) {
            this.cam_id = this.props.match.params.id;
        }

        this.db = new AirDB();
        this.db.cam_id = this.cam_id;

        this.state = { "dbData": [] };
        this.db.callback = () => {
            this.setState({ "dbData": this.db.data });
            console.log(this.db.data);
        };
    }

    componentDidMount() {
        // this.db.connectToSocket();
        this.db.startPolling();
    }

    render() {
        var panels;
        if (this.state.dbData.length > 0) {
            let sorted = this.state.dbData.slice(-1)[0].species.slice().sort((a, b) => {
                let aIndex = this.priorities.indexOf(a.name);
                let bIndex = this.priorities.indexOf(b.name);
                if (aIndex == -1) {aIndex = this.priorities.length + 5}
                if (bIndex == -1) {bIndex = this.priorities.length + 5}
                return aIndex - bIndex;
            });
            panels = sorted.map((spec) => {
                return (
                    <Grid item xs={12} md={4} sm={6} key={ spec.name }>
                        <GasPanel data={ spec }/>
                    </Grid>
                );
            });
        } else {
            panels = <p></p>
        }

        var stationName = "Loading Data..."
        if (this.state.dbData.slice(-1)[0]) {
            stationName = this.state.dbData.slice(-1)[0].station
        }

        var dev_id = "CAM " + this.cam_id;
        console.log(this.state.dbData.slice(-1)[0])
        if (this.state.dbData.slice(-1)[0]) {
            if (this.state.dbData.slice(-1)[0].name) {
                dev_id = this.state.dbData.slice(-1)[0].name;
            }
        }

        return (
            <div className="socket_container">
                <Typography variant="display2">
                    { dev_id }
                </Typography>
                <Typography variant="display1" gutterBottom>
                    { stationName }
                </Typography>
                <Grid container spacing={16}>
                    { panels }
                </Grid>
                <Grid container>
                    <Grid item xs={6}>
                        <a href="https://itunes.apple.com/us/app/aqtreks/id1278345384?mt=8">
                            <img src={AppleStoreBadge} className="store_badge apple_badge"/>
                        </a>
                    </Grid>
                    <Grid item xs={6}>
                        <a href="https://play.google.com/store/apps/details?id=go3treks.craig.com.go3treks">
                            <img src={AndroidStoreBadge} className="store_badge"/>
                        </a>
                    </Grid>
                </Grid>
            </div>
        );
    }
}
