export class Species {

    static DisplayForSymbol(symbol) {
        let arr = SpeciesJSON.symbols[symbol];
        return Species.DisplayForSpecies(arr[0])
    };

    static UnitsForSymbol(symbol) {
        let arr = SpeciesJSON.symbols[symbol];
        return Species.DispalyForUnits(arr[1]);
    }

    static DisplayForSpecies(spec) {
        return SpeciesJSON.species[spec].disp;
    }

    static DispalyForUnits(unit) {
        return SpeciesJSON.units[unit].disp;
    }

    static AQIBreaksForSpecies(species) {
        for (let spec in SpeciesJSON.species) {
            let model = SpeciesJSON.species[spec];
            if (spec === species || species === model.disp || species in model.aliases) {
                if ('aqi' in model) {
                    for (var key in model.aqi) {
                        return model.aqi[key];
                    }
                } else {
                    return null;
                }
            }
        }
        return null;
    }
};

export var SpeciesJSON = 
{ "notes_about_this_file": [

    " !! ALWAYS VALIDATE THIS FILE BEFORE PUSHING TO PRODUCTION SERVERS !! ",
    
    "This file can be validated by running species.py standalone",
    "(i.e., `python3 species.py`).  See the comments in species.py for",
    "more information about specifying species and units.  It is important",
    "that species keys, aliases, and symbols are all unique to prevent",
    "mis-labeling of units.",
    
    " !! DO NOT ALTER EXISTING ALIASES/SYMBOLS/KEYS !! ",
    
    "If new data inputs specify units or species that collide with current",
    "entries, add a unique identifier and an alias which is used instead by",
    "the source inlet processing step.  There are no safeguards in place to",
    "prevent future mis-labeling of existing database data if you alter",
    "existing species/unit keys and aliases.",
    
    " !! ALWAYS SAVE THIS FILE AS UTF-8 !! ",
    
    "See species.py documentation for information on how aliases and symbols",
    "work."
    
    ],
    
    "species": {
        "device": {
            "note": "This entry is NOT for storing data and should only be used to pass instrument IDs in datalines.",
            "disp": "Device",
            "desc": "Device ID",
            "aliases": ["dev"],
            "units": ["int"]
        },
        "posix": {
            "note": "This entry is NOT for storing data and should only be used to pass timestamps in datalines.",
            "disp": "POSIX Timestamp",
            "desc": "POSIX Timestamp",
            "aliases": [],
            "units": ["posix"]
        },
        "zero": {
            "note": "This entry will be assumed false if omitted.  True means the instrument is in zero mode.",
            "disp": "Zero mode",
            "desc": "True if the instrument was self-zeroing",
            "aliases": [],
            "units": ["bool"]
        },
        "diagnostic": {
            "note": "This entry will be assumed '0' if omitted.  Instrument diagnostic codes.",
            "disp": "Diagnostic",
            "desc": "Diagnostic code",
            "aliases": [],
            "units": ["int"]
        },
        "batt": {
            "disp": "Battery",
            "desc": "Instrument battery",
            "aliases": ["battery"],
            "units": ["volt","perc"]
        },
        "lat": {
            "disp": "Latitude",
            "desc": "Latitude",
            "aliases": ["latitude"],
            "units": ["deg"]
        },
        "lon": {
            "disp": "Longitude",
            "desc": "Longitude",
            "aliases": ["longitude"],
            "units": ["deg"]
        },
        "temp": {
            "disp": "Temperature",
            "desc": "Temperature",
            "aliases": ["temperature"],
            "units": ["fahr","cels"]
        },
        "press": {
            "disp": "Pressure",
            "desc": "Air pressure",
            "aliases": ["pressure"],
            "units": ["mbar"]
        },
        "rhum": {
            "disp": "Humidity",
            "desc": "Relative humidity",
            "aliases": ["hum","humidity","relhum"],
            "units": ["perc"]
        },
        "sound": {
            "disp": "Sound",
            "desc": "Sound volume",
            "aliases": ["snd"],
            "units": ["db"]
        },
        "o3": {
            "disp": "Ozone",
            "desc": "Ozone",
            "aliases": ["ozone","o₃"],
            "units": ["ppb","ppm","perc"],
            "aqi": {"ppb":[0, 54, 70, 85, 105, 404, 604]}
        },
        "co2": {
            "disp": "CO₂",
            "desc": "Carbon dioxide",
            "aliases": ["carbon dioxide","co₂"],
            "units": ["ppb","ppm","perc"],
            "aqi": {"ppm":[0, 600, 1000, 2500, 5000, 6000, 6500]}
        },
        "co": {
            "disp": "CO",
            "desc": "Carbon monoxide",
            "aliases": ["carbon monoxide"],
            "units": ["ppb","ppm","perc"],
            "aqi": {"ppm":[0, 4.4, 9.4, 12.4, 15.4, 30.4, 50.4]}
        },
        "pm1": {
            "disp": "PM, 1µm",
            "desc": "Particulate matter, 1 micron",
            "aliases": ["pm 1"],
            "units": ["ugm3"],
            "aqi": {"ugm3":[0, 5, 30, 50, 100, 150, 200]}
        },
        "pm2.5": {
            "disp": "PM, 2.5µm",
            "desc": "Particulate matter, 2.5 micron",
            "aliases": ["pm 2.5"],
            "units": ["ugm3"],
            "aqi": {"ugm3":[0, 12, 35.4, 55.4, 150.4, 250.4, 500.4]}
        },
        "pm10": {
            "disp": "PM, 10µm",
            "desc": "Particulate matter, 10 micron",
            "aliases": ["pm 10"],
            "units": ["ugm3"],
            "aqi": {"ugm3":[0, 54, 154, 254, 354, 424, 604]}
        },
        "voc": {
            "disp": "VOC",
            "desc": "Volatile Organic Carbon",
            "aliases": ["vocs"],
            "units": ["iaq","ohm"]
        },
        "no2": {
            "disp": "NO₂",
            "desc": "Nitrogen dioxide",
            "aliases": ["nitrogen dioxide"],
            "units": ["ppb","ppm","perc"],
            "aqi": {"ppb":[0, 53, 100, 360, 649, 1249, 2049]}
        },
        "so2": {
            "disp": "SO₂",
            "desc": "Sulfur dioxide",
            "aliases": ["sulfur dioxide"],
            "units": ["ppb","ppm","perc"],
            "aqi": {"ppb":[0, 35, 75, 185, 304, 604, 1004]}
        }
    },
    
    "units": {
        "int": {
            "disp": "",
            "desc": "Integer numeric value",
            "aliases":["integer"]
        },
        "bool": {
            "disp": "(True=1)",
            "desc": "True/false (0 = false, 1 = true)",
            "aliases":["boolean"]
        },
        "posix": {
            "disp": "seconds",
            "desc": "POSIX timestamp (seconds since midnight UTC, January 1, 1970)",
            "aliases":[]
        },
        "deg": {
            "disp": "°",
            "desc": "Degrees",
            "aliases":["degrees","angle"]
        },
        "ppb": {
            "disp": "ppb",
            "desc": "parts per billion",
            "aliases":["parts per billion"]
        },
        "ppm": {
            "disp": "ppm",
            "desc": "parts per million",
            "aliases":["parts per million"]
        },
        "perc": {
            "disp": "%",
            "desc": "percent",
            "aliases":["%", "percent", "/ 100", "/100"]
        },
        "ug": {
            "disp": "µg",
            "desc": "micrograms",
            "aliases":["microgram", "micrograms", "µg"]
        },
        "cels": {
            "disp": "°C",
            "desc": "Degrees Celsius",
            "aliases":["c", "celsius", "centigrade", "°c"]
        },
        "fahr": {
            "disp": "°F",
            "desc": "Degrees Fahrenheit",
            "aliases":["f", "fahrenheit", "°f"]
        },
        "ugm3": {
            "disp": "µg/m³",
            "desc": "Micrograms per cubic meter",
            "aliases":["ug/m3", "ug\\/m3", "µg/m³", "µgm³"]
        },
        "mbar": {
            "disp": "mbar",
            "desc": "Millibar",
            "aliases":["mb", "millibar"]
        },
        "volt": {
            "disp": "V",
            "desc": "Volts",
            "aliases":["v", "volts"]
        },
        "db": {
            "disp": "dB",
            "desc": "decibels",
            "aliases":["decibel", "decibels"]
        },
        "ohm": {
            "disp": "Ω",
            "desc": "Ohms",
            "aliases":["ohms", "Ω"]
        },
        "iaq": {
            "disp": "IAQ",
            "desc": "Indoor Air Quality (0-500, 0 best)",
            "aliases":[]
        }
    },
    
    "symbols": {
        "Z": ["device", "int"],
        "Y": ["posix", "posix"],
        "X": ["batt", "volt"],
        "x": ["batt", "perc"],
        "W": ["diagnostic", "int"],
        "z": ["zero", "bool"],
        
        "a": ["lat", "deg"],
        "o": ["lon", "deg"],
        "t": ["temp", "cels"],
        "P": ["press", "mbar"],
        "h": ["rhum", "perc"],
        "s": ["sound", "db"],
        "O": ["o3", "ppb"],
        "C": ["co2", "ppm"],
        "M": ["co", "ppm"],
        "r": ["pm1", "ugm3"],
        "R": ["pm2.5", "ugm3"],
        "q": ["pm10", "ugm3"],
        "g": ["voc", "iaq"]
    }
    
}