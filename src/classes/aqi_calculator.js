import { Species } from "./species";

export var indexBreak = [0, 50, 100, 150, 200, 300, 500]

export var AQI_Enum = Object.freeze({
    "DNE": "DNE",
    "Error": "Error",
    "Satisfactory": "Satisfactory",
    "Moderate": "Moderate",
    "Poor": "Poor",
    "VeryPoor": "VeryPoor",
    "Severe": "Severe",
    "Hazardous": "Hazardous",
});

export var AQI_Colors = Object.freeze({
    "DNE": {
        primary: "#0080FF",
        secondary: "#0F52BA",
    },
    "Satisfactory": {
        primary: "#00E800",
        secondary: "#00B200",
    },
    "Moderate": {
        primary: "#FFFF00",
        secondary: "#FFCC00",
    },
    "Poor": {
        primary: "#FF7E00",
        secondary: "#BE5E00",
    },
    "VeryPoor": {
        primary: "#FF0000",
        secondary: "#BE0000",
    },
    "Severe": {
        primary: "#8F3F97",
        secondary: "#5F2964",
    },
    "Hazardous": {
        primary: "#7E0023",
        secondary: "#5B0019",
    },
    
});

export function AQI_Raw(value, gas) {
    let breaks = Species.AQIBreaksForSpecies(gas);
    if (breaks == null) { return -2; }
    
    var breakIndex = 0;
    for (var i = 0; i < breaks.length; i++) {
        let val = breaks[i];
        if (val > value) {
            break;
        }
        breakIndex += 1;
    }
    if (breakIndex >= breaks.length) { return 999 }
    if (breakIndex === 0) {
        return 0;
    }
    
    let aqi = (((indexBreak[breakIndex] - indexBreak[breakIndex-1]) / (breaks[breakIndex] - breaks[breakIndex-1])) * (value - breaks[breakIndex-1])) + indexBreak[breakIndex-1];
    return aqi;
}

export function AQI(value, gas) {
    let aqi = AQI_Raw(value, gas);

    if (aqi === -2) {
        return AQI_Enum.DNE;
    } else if (aqi === -1) {
        return AQI_Enum.Error;
    } else if (aqi <= 50) {
        return AQI_Enum.Satisfactory;
    } else if (aqi <= 100) {
        return AQI_Enum.Moderate;
    } else if (aqi <= 150) {
        return AQI_Enum.Poor;
    } else if (aqi <= 200) {
        return AQI_Enum.VeryPoor;
    } else if (aqi <= 300) {
        return AQI_Enum.Severe;
    } else {
        return AQI_Enum.Hazardous;
    }
}

export function AQI_Color(value, gas) {
    return AQI_Colors[AQI(value, gas)].primary;
}

