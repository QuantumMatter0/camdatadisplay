const io = require("socket.io")();

io.on('connection', (client) => {
    console.log("Client Connected!");
    client.on("subscribeToCAMData", (id) => {
        setInterval(() => {
            let data = [
                {
                    "name": "CAM " + id,
                    "species": [
                        {
                            "name": "CO2",
                            "units": "PPM",
                            "points": [
                                {
                                    "value": Math.floor(Math.random() * 300),
                                    "timestamp": new Date(),
                                }
                            ],
                        },
                        {
                            "name": "CO",
                            "units": "PPB",
                            "points": [
                                {
                                    "value": Math.floor(Math.random() * 300),
                                    "timestamp": new Date(),
                                }
                            ],
                        },
                        {
                            "name": "Temp",
                            "units": "C",
                            "points": [
                                {
                                    "value": Math.floor(Math.random() * 300),
                                    "timestamp": new Date(),
                                }
                            ],
                        },
                        {
                            "name": "PM1",
                            "units": "ug/m3",
                            "points": [
                                {
                                    "value": Math.floor(Math.random() * 300),
                                    "timestamp": new Date(),
                                }
                            ],
                        },
                    ],
                }
            ];

            client.emit("new_cam_data", JSON.stringify(data));
        }, 2500);
    });
});

const PORT = 8000;
io.listen(PORT);

console.log("Started Socket.IO server on ", PORT);