import openSocket from "socket.io-client";
import Axios from "axios";

import { Species } from "./species";

// export class AQData {
//     value;
//     timestamp;
// }

// export class AQSpecies {
//     name;
//     units;
//     points;
// }

// export class AQDevice {
//     name;
//     species;
// }

export class AirDB {

    data = [];
    cam_id = 22;

    callback = () => { console.log("DEFAULT AirDB CALLBACK!"); };

    socketConnected = false;
    isPolling = false;

    connectToSocket() {
        if (!this.socketConnected) {
            this.socket = openSocket("web3.citizenair.org:8000");

            this.socket.on("new_cam_data", (cam) => {
                this.handleSocket(cam);
            });
    
            this.socket.emit("subscribeToCAMData", 99);

            this.socketConnected = true;
        }
    }

    handleSocket(cam) {
        for(let cam_index = 0; cam_index < cam.length; cam_index++) {

            var device_index = -1;
            for(let i = 0; i < this.data.length; i++) {
                if (this.data[i].name ===  cam[cam_index].name) {
                    device_index = i;
                    break;
                }
            }

            if (device_index === -1) {
                // console.log("Adding Device!");
                this.data.push(cam[cam_index]);
                continue;
            }

            for(let cam_spec_index = 0; cam_spec_index < cam[cam_index].species.length; cam_spec_index++) {
                // console.log(device_index, device, device.species, cam[cam_index].species);
                let index = -1;
                for(let i = 0; i < this.data[device_index].species.length; i++) {
                    if (this.data[device_index].species[i].name === cam[cam_index].species[cam_spec_index].name) {
                        index = i;
                        break;
                    }
                }

                if (index === -1) {
                    // console.log("Added new species!");
                    this.data[device_index].push(cam.species[cam_spec_index]);
                    continue;
                }

                this.data[device_index].species[index].points = this.data[device_index].species[index].points.concat(cam[cam_index].species[cam_spec_index].points);
                // console.log("Updating species data!");
            }
        }

        this.callback();
    }

    startPolling() {
        if (!this.isPolling) {
            this.isPolling = true;
            this.poll();
        }
    }

    poll() {
        Axios.post("http://web1.airqdb.com/a/1", {
            "m": "query_cam",
            "p": {
                "cam_id": this.cam_id,
                // "send_species": true,
            }
        })
        .then((response) => {
            this.handleHTTP(response.data);
        })
        .catch((error) => {
            console.log(error);
        });

        if (this.isPolling) {
            setTimeout(() => {
                this.poll();
            }, 4000);
        }
    }

    handleHTTP(response) {

        let id = response.latest.dev_id;
        let point_id = response.latest.point_id;

        var cam_index = -1;
        for (var i = 0; i < this.data.length; i++) {
            if (this.data[i].name === ("CAM " + id)) {
                cam_index = i;
                break;
            }
        }

        console.log("Response: ", response);
        if (cam_index === -1) {
            cam_index = this.data.length;
            this.data.push({
                "name": "CAM " + id,
                "station": response.station_name,
                "species": [],
            });
        }

        let species = response.latest.species;
        for (var resp_index = 0; resp_index < species.length; resp_index++) {

            var spec_index = -1;
            var disp = Species.DisplayForSpecies(species[resp_index].s);//spec_spec[species[resp_index].s].disp;
            var units = Species.DispalyForUnits(species[resp_index].u); //spec_spec[species[resp_index].s].units;
            for (var i = 0; i < this.data[cam_index].species.length; i++) {
                if (this.data[cam_index].species[i].name == disp) {
                    spec_index = i;
                    break;
                }
            }

            if (spec_index === -1) {
                spec_index = this.data[cam_index].species.length;
                this.data[cam_index].species.push({
                    "name": disp, //species[resp_index].s,
                    "units": units,//species[resp_index].u,
                    "points": [],
                });
            }

            if (this.data[cam_index].species[spec_index].last_point == point_id) {
                break;
            }

            this.data[cam_index].species[spec_index].last_point = point_id;
            this.data[cam_index].species[spec_index].points.push({
                "value": species[resp_index].v,
                "timestamp": new Date(response.latest.ts),
            })
        }

        console.log(this.data);
        this.callback();
    }
}

export var data = {
    "devices": [
        {
            "name": "PAM 99",
            "data": [
                {
                    "species":"CO2",
                    "units":"PPM",
                    "points": [
                        {
                            "value": 200,
                            "timestamp": new Date(),
                        },
                    ]
                },
                {
                    "species":"PM10",
                    "units":"ug/m3",
                    "points": [
                        {
                            "value": 200,
                            "timestamp": new Date(),
                        },
                    ]
                },
                {
                    "species":"PM1",
                    "units":"ug/m3",
                    "points": [
                        {
                            "value": 200,
                            "timestamp": new Date(),
                        },
                    ]
                },
                {
                    "species":"PM2.5",
                    "units":"ug/m3",
                    "points": [
                        {
                            "value": 200,
                            "timestamp": new Date(),
                        },
                    ]
                },
                {
                    "species":"CO",
                    "units":"ug/m3",
                    "points": [
                        {
                            "value": 200,
                            "timestamp": new Date(),
                        },
                    ]
                },
                {
                    "species":"Temp",
                    "units":"ug/m3",
                    "points": [
                        {
                            "value": 200,
                            "timestamp": new Date(),
                        },
                    ]
                },
            ]
        },
    ],
}

export function update(cb) {
    for (var i = 0; i < data.devices.length; i++) {
        let device = data.devices[i];
        for (var j = 0; j < device.data.length; j++) {
            var deviceData = device.data[j];
            deviceData.points.push({
                "value": 200 + Math.floor(Math.random() * 100),
                "timestamp": new Date(),
            });
            data.devices[i].data[j].points = deviceData.points;
        }
    }
    cb();
    setTimeout(() => { update(cb); }, 5000);
}