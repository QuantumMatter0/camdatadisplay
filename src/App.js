import React, { Component } from 'react';
import './App.css';

import Grid from "@material-ui/core/Grid";

import { GasPanel } from "./components/GasPanel/GasPanel";
import { data, update } from "./classes/airdb";

class App extends Component {

  constructor(props) {
    super(props);

    this.state = { "devices": [{ "data": [] }] } ;
  }

  componentDidMount() {
    update(() => { this.setState(data) });
  }

  render() {
    var panels = this.state.devices.slice(-1)[0].data.map((spec_data) => {
      return (
        <Grid item xs={12} md={3} sm={6} key={ spec_data.species } >
          <GasPanel data={ spec_data } />
        </Grid>
      )
    });

    return (
      <div className="App">
        <Grid container>
          { panels }
        </Grid>
      </div>
    );
  }
}

export default App;
